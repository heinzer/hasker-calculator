package test;
import static org.junit.Assert.*;
import org.junit.*;
import calculator.Calculator;
import calculator.Calculator.*;


public class CalculatorTest {
	
	private Calculator calculator;
	private int delta = 0;
	
	@Before
	public void Init(){
		calculator = new Calculator();
	}
	
	//****************Addition****************
	
	/*
	 * @author Erin
	 */
	@Test
	public void Add_PositiveNumbers() {
		int[] numbers = new int[]{0, 0, 0, 0, 0, 0, 0};
		assertEquals(0, calculator.add(numbers), delta);
		
		numbers = new int[]{3, 4};
		assertEquals(7, calculator.add(numbers), delta);
		
		numbers = new int[]{3, 4, 5};
		assertEquals(12, calculator.add(numbers), delta);
		
		numbers = new int[]{3, 4, 5, 6, 7, 8, 9, 10};
		assertEquals(52, calculator.add(numbers), delta);
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Add_NegativeNumbers() {
		int[] numbers = new int[]{-3, -4};
		assertEquals(-7, calculator.add(numbers), delta);
		
		numbers = new int[]{-3, 4, -5};
		assertEquals(-4, calculator.add(numbers), delta);
	}
	
	//****************Subtraction****************
	
	/*
	 * @author Erin
	 */
	@Test
	public void Subtract_PositiveNumbers() {
		int[] numbers = new int[]{0, 0, 0, 0, 0, 0, 0};
		assertEquals(0, calculator.subtract(numbers), delta);
		
		numbers = new int[]{7, 4};
		assertEquals(3, calculator.subtract(numbers), delta);
		
		numbers = new int[]{12, 4, 5};
		assertEquals(3, calculator.subtract(numbers), delta);
		
		numbers = new int[]{52, 4, 5, 6, 7, 8, 9, 10};
		assertEquals(3, calculator.subtract(numbers), delta);
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Subtract_NegativeNumbers() {
		int[] numbers = new int[]{-3, -4};
		assertEquals(1, calculator.subtract(numbers), delta);
		
		numbers = new int[]{-3, 4, -5};
		assertEquals(-2, calculator.subtract(numbers), delta);
	}
	

	
	//****************Multiplication****************
	
	/*
	 * @author Erin
	 */
	@Test
	public void Multiply_PositiveNumbers(){
		int[] numbers = new int[]{1, 1};
		assertEquals(1, calculator.multiply(numbers), delta);
		
		numbers = new int[]{0, 1};
		assertEquals(0, calculator.multiply(numbers), delta);
		
		numbers = new int[]{1, 2, 3};
		assertEquals(6, calculator.multiply(numbers), delta);
		
		numbers = new int[]{1, 2, 3, 4};
		assertEquals(24, calculator.multiply(numbers), delta);
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Multiply_NegativeNumbers(){
		int[] numbers = new int[]{-1, -1};
		assertEquals(1, calculator.multiply(numbers), delta);
		
		numbers = new int[]{0, -1};
		assertEquals(0, calculator.multiply(numbers), delta);
		
		numbers = new int[]{-1, -2, -3};
		assertEquals(-6, calculator.multiply(numbers), delta);
		
		numbers = new int[]{-1, -2, -3, -4};
		assertEquals(24, calculator.multiply(numbers), delta);
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Multiply_PositiveAndNegativeNumbers(){
		int[] numbers = new int[]{1, -1};
		assertEquals(-1, calculator.multiply(numbers), delta);
		
		numbers = new int[]{0, -1};
		assertEquals(0, calculator.multiply(numbers), delta);
		
		numbers = new int[]{-1, -2, 3};
		assertEquals(6, calculator.multiply(numbers), delta);
		
		numbers = new int[]{-1, -2, -3, 4};
		assertEquals(-24, calculator.multiply(numbers), delta);
	}	

	//****************Division****************
	
	/*
	 * @author Erin
	 */
	@Test(expected=Exception.class)
	public void Divide_ReturnsError() throws Exception {
		
		int[] numbers = new int[]{1, 0};
		calculator.divide(numbers);
	}	
	
	/*
	 * @author Erin
	 */
	@Test
	public void Divide_PositiveNumbers(){
		try {
			int[] numbers = new int[]{1, 1};
			assertEquals(1, calculator.divide(numbers), delta);
			
			numbers = new int[]{24, 6};
			assertEquals(4, calculator.divide(numbers), delta);
			
			numbers = new int[]{100, 5, 5};
			assertEquals(4, calculator.divide(numbers), delta);
		} catch (Exception e) {
			fail("Should not have gotten here");
		}
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Divide_NegativeNumbers(){
		try {
			int[] numbers = new int[]{-1, -1};
			assertEquals(1, calculator.divide(numbers), delta);
			
			numbers = new int[]{-24, -6};
			assertEquals(4, calculator.divide(numbers), delta);
			
			numbers = new int[]{-100, -5, -5};
			assertEquals(-4, calculator.divide(numbers), delta);
		} catch (Exception e) {
			fail("Should not have gotten here");
		}
	}
	
	/*
	 * @author Erin
	 */
	@Test
	public void Divide_PositiveAndNegativeNumbers(){
		try {
			int[] numbers = new int[]{-1, 1};
			assertEquals(-1, calculator.divide(numbers), delta);
			
			numbers = new int[]{-24, 6};
			assertEquals(-4, calculator.divide(numbers), delta);
			
			numbers = new int[]{-100, 5, -5};
			assertEquals(4, calculator.divide(numbers), delta);
		} catch (Exception e) {
			fail("Should not have gotten here");
		}
	}

	//****************DisplayHistory****************
	/**
	 * @author minorr
	 */
	@Test
	public void displayHistoryTwoConsecutiveInputs(){
	    try {
	        int[] numbers = new int[]{3, 1};
	        calculator.executeCommand(Command.ADD, numbers);
	        calculator.executeCommand(Command.SUB, numbers);
            assertEquals("add 3 1 = 4\nsub 3 1 = 2\n", calculator.displayHistory());
        } catch (Exception e) {
            fail("Should not have gotten here");
        }
	}
	
	//****************ClearHistory****************
    /**
     * @author minorr
     */
    @Test
    public void clearHistoryTest(){
        try {
            int[] numbers = new int[]{3, 1};
            calculator.executeCommand(Command.ADD, numbers);
            calculator.clearHistory();
            assertEquals("", calculator.displayHistory());
        } catch (Exception e) {
            fail("Should not have gotten here");
        }
    }

    //****************AddToHistory****************
    /**
     * @author minorr
     */
    @Test
    public void addOnlyToHistory(){
        try {
            int[] numbers = new int[]{3, 1};
            calculator.addToHistory(Command.ADD, "4", numbers);
            assertEquals("add 3 1 = 4\n", calculator.displayHistory());
        } catch (Exception e) {
            fail("Should not have gotten here");
        }
    }
    //****************GetHistoryResult****************
    /**
     * @author minorr
     */
    @Test
    public void getHistoryResultSubOne(){
        int[] numbers = new int[]{3,1};
        calculator.addToHistory(Command.ADD, "4", numbers);
        calculator.addToHistory(Command.SUB, "2", numbers);
        calculator.addToHistory(Command.MUL, "3", numbers);
        assertEquals("5", calculator.parseInput("add !1 1"));
    }
    
    /**
     * @author minorr
     */
    @Test
    public void getHistoryResultSubTwo(){
        int[] numbers = new int[]{3,1};
        calculator.addToHistory(Command.ADD, "4", numbers);
        calculator.addToHistory(Command.SUB, "2", numbers);
        calculator.addToHistory(Command.MUL, "3", numbers);
        assertEquals("6", calculator.parseInput("add !1 !2"));
    }
    
    /**
     * @author minorr
     */
    @Test
    public void getHistoryResultSubMissing(){
        int[] numbers = new int[]{3,1};
        calculator.addToHistory(Command.ADD, "4", numbers);
        calculator.addToHistory(Command.SUB, "2", numbers);
        calculator.addToHistory(Command.MUL, "3", numbers);
        assertEquals("Unable to parse input", calculator.parseInput("add !5 1"));
    }
	//****************ParseInput****************
    /**
     * @author heinzer
     */
    @Test
    public void ParseInput_InvalidCommand(){
    	assertEquals("Unable to parse input", calculator.parseInput("1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("DERP 1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("DR 1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("DER 1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("ADD - 1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("ADD A 1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("ADD +1 2 3"));
    	assertEquals("Unable to parse input", calculator.parseInput("   ADD +1 2 3"));

    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ParseInput_ValidCommand(){
    	assertEquals("6", calculator.parseInput("ADD 1 2 3"));
    	assertEquals("4", calculator.parseInput("ADD -1 2 3 "));
    	assertEquals("6", calculator.parseInput("ADD 1,2,3"));
    	assertEquals("", calculator.parseInput("CLR 1,2,3"));
    	assertEquals("", calculator.parseInput("DPL 1,2,3")); 	
    }
    

    
    //****************ExecuteCommand****************
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_Add(){
    	assertEquals("6", calculator.executeCommand(Command.ADD, new int[]{4, 2}));    	
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_Subtract(){
    	assertEquals("2", calculator.executeCommand(Command.SUB, new int[]{4, 2}));    	
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_Multiply(){
    	assertEquals("8", calculator.executeCommand(Command.MUL, new int[]{4, 2}));    	
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_Divide(){
    	assertEquals("2.0", calculator.executeCommand(Command.DIV, new int[]{4, 2}));   
    	assertEquals("Error: divide by zero", calculator.executeCommand(Command.DIV, new int[]{4, 0}));  
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_Factorial(){
    	assertEquals("24", calculator.executeCommand(Command.FAC, new int[]{4}));    	
    	assertEquals("24", calculator.executeCommand(Command.FAC, new int[]{4, 8, 7}));   
    	assertEquals("1", calculator.executeCommand(Command.FAC, new int[]{0}));   
    	assertEquals("Can not find factorial of a negative number", calculator.executeCommand(Command.FAC, new int[]{-4}));   
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_DisplayHistory(){
    	assertEquals("", calculator.executeCommand(Command.DPL, new int[]{}));  
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_ClearHistory(){
    	calculator.addToHistory(Command.ADD, "4", new int[]{2, 2});
    	assertEquals("", calculator.executeCommand(Command.CLR, new int[]{}));  
    	assertEquals("", calculator.displayHistory()); 
    	
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void ExecuteCommand_CallsAddToHistory(){
    	calculator.executeCommand(Command.ADD, new int[]{2, 2});
    	assertEquals("add 2 2 = 4 ".length(), calculator.displayHistory().length()); 
    }
    
    //****************Factorial****************
    
    /**
     * @author heinzer
     */
    @Test
    public void Factorial_PositiveInteger(){
    	assertEquals(1, calculator.factorial(new int[]{0}));
    	assertEquals(24, calculator.factorial(new int[]{4}));
    }
    
    /**
     * @author heinzer
     */
    @Test
    public void Factorial_MultiplePositiveIntegers(){
    	assertEquals(1, calculator.factorial(new int[]{0, 0, 0}));
    	assertEquals(24, calculator.factorial(new int[]{4, 4, 4}));
    }
}
