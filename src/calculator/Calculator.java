package calculator;

import java.util.ArrayList;
import java.util.Scanner;

public class Calculator {
	
	protected ArrayList<String> history;
    
	/**
	 * @author minorr
	 * @param args
	 */
    public static void main(String[] args) {
    	Calculator calc = new Calculator();
        Scanner scanIn = new Scanner(System.in);
        String input = "";
        System.out.println("Please input a command");
        
        while(!input.equals("ext")){
            input = scanIn.nextLine();
            System.out.println(calc.parseInput(input));
        }
        System.out.println("Exited");
    }
    
    /**
     * @author minorr
     */
    public Calculator(){
        history = new ArrayList<String>();
    }
    
    public enum Command {
        ERROR, ADD, SUB, MUL, DIV, FAC, CLR, DPL;
    }

    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public String parseInput(String input){
        //Strip command
        String sCommand = input.substring(0, 3);
        Command command;
        try {
            command = Command.valueOf(sCommand.toUpperCase());
        } catch (Exception e1) {
            command = Command.ERROR;
        }
        
        int[] inputs = null;
        
        //Take anything after the command and split on any sized portion of non-digit characters
        if(!(command.equals(Command.CLR) || command.equals(Command.DPL) || command.equals(Command.ERROR))){
            String[] numbers = input.substring(3).trim().split("[^0-9!\\-]+");
            inputs = new int[numbers.length];
            
            try {
                for(int i = 0; i< numbers.length; i++){
                    if(!numbers[i].contains("!")){
                        //If it is just a number, parse
                        inputs[i] = Integer.parseInt(numbers[i]);
                    }else{
                        //If it is a code to sub in a number from history, get that result by calling getHistoryResult with just the integer
                        inputs[i] = Integer.parseInt(getHistoryResult(Integer.parseInt(numbers[i].replaceAll("[^0-9-]", ""))));
                    }
                }
            } catch (NumberFormatException e) {
                command = Command.ERROR;
            }
        }
        
        return executeCommand(command, inputs);
    }
    
    /**
     * @author heinzer
     * @param command
     * @param output
     * @param inputs
     * @return
     */
    public String executeCommand(Command command, int[] inputs){
        String output = "";
    	switch (command){
            case ERROR:
                output = "Unable to parse input";
                break;
            case ADD:
                output += add(inputs);
                addToHistory(command, output, inputs);
                break;
            case SUB:
                output += subtract(inputs);
                addToHistory(command, output, inputs);
                break;
            case MUL:
                output += multiply(inputs);
                addToHistory(command, output, inputs);
                break;
            case DIV:
                try {
                    output += divide(inputs);
                    addToHistory(command, output, inputs);
                } catch (Exception e) {
                    output = "Error: divide by zero";
                }
                break;
            case FAC:
                if(inputs[0] >= 0){
                    output += factorial(inputs);
                    addToHistory(command, output, inputs);
                }else{
                    output = "Can not find factorial of a negative number";
                }
                break;
            case DPL:
            	output = displayHistory();
            	break;
            case CLR:
            	clearHistory();
            	break;
            default:
                output = "Unknown command";
                break;
        }
    		
        return output;
    }
    
    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public int add(int[] inputs){
        int sum = 0;
        
        for (int i : inputs){
            sum += i;
        }
        
        return sum;
    }
    
    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public int subtract(int[] inputs){
        int sum = inputs[0];
        
        for(int i = 1; i<inputs.length; i++){
        	sum -= inputs[i];
        }
        
        return sum;
    }
    
    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public int multiply(int[] inputs){
        int product = 1;
        
        for (int i : inputs){
            product *= i;
        }
        
        return product;
    }
    
    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public float divide(int[] inputs) throws Exception{
        float quot = inputs[0];
        
        for(int i = 1; i < inputs.length; i++){
            if(inputs[i] != 0){
            	quot /= inputs[i];
            } else{
            	throw new Exception("Can't divide by zero");
            }
        }
        
        return quot;
    }
    
    /**
     * @param inputs
     * @author minorr
     * @return
     */
    public int factorial(int[] inputs){
        int counter = inputs[0];
        int product = 1;
        
        while (counter>1){
            product *= counter;
            counter--;
        }
        return product;
    }
    
    /**
     * @author heinzer
     */
    public String displayHistory(){
    	String result = "";
    	for(String h : history){
    		result = result + h + "\n";
    	}
    	
    	return result;
    }
    
	/**
	 * @author heinzer
	 */
    public void clearHistory(){
    	history = new ArrayList<String>();
    }

    /**
     * @author heinzer
     * @param command
     * @param output
     * @param inputs
     */
    public void addToHistory(Command command, String output, int[] inputs){
    	switch(command){
		case ERROR:
			break;
		default:				
			String in = "";
			for(int i : inputs){
				in = in + i + " ";
			}
			history.add(command.toString().toLowerCase() + " " + in + "= " + output);
		}
    }
    
    /**
     * @author heinzer
     * @param index
     * @return
     */
    public String getHistoryResult(int index){
    	if(index < 0 || index > history.size())
    		return "Index out of bounds";
    	return history.get(index-1).split(" = ")[1];
    }
}