This is a calculator project that provides the functionality to perform mathematical calculations.
Group Members: Erin Heinz and Ryan Minor
Java Version: 1.8.0_66
(The ANT is using 1.7)
JUnit Version: 4.2
The build being used was the one created in ANT, then modified to specify 1.7 target/source versions
Simply typing `ant` in /hasker-calculator runs the ant test and reports build successful. We know tests work because of the lines:
CalculatorTest:
    [junit] Running test.CalculatorTest
    [junit] Tests run: 29, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.055 sec